<?php
/**
 * The template for displaying the footer.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
?>

<footer id="site-footer">
</footer>

<?php wp_footer(); ?>

</body>
</html>