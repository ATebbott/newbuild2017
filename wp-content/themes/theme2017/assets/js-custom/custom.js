$(document).ready(function() {
	//Pull through all scripts in customScripts function
   	customScripts();

});


//Primary scripts function
function customScripts() {


	/*  ==========================================================================
		Custom Events
		========================================================================== */
		//Initialise Animate on Scroll
	  	AOS.init({
		  	startEvent: 'load'
		});

	    if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
	     	var evt = document.createEvent('UIEvents');
	     	evt.initUIEvent('resize', true, false, window, 0);
	     	window.dispatchEvent(evt);
	    } else {
	       	window.dispatchEvent(new Event('resize'));

	    }

		
	/*  ==========================================================================
		jQuery Plugins
		========================================================================== */	

		//Slickslider
		// $('.team-carousel').slick({
		//   	infinite: true,
		//   	slidesToShow: 6,
		//   	slidesToScroll: 6,
		//   	centerMode: true,
		//   	prevArrow : '<div class="slick-prev"><i></i></div>',
		//     nextArrow : '<div class="slick-next"><i></i></div>'
		// });



		//Matchheight.js	
		// $('.pricing-content-mh').matchHeight({ remove: true });
		// $('.pricing-content-mh').matchHeight();
		
		// $.fn.matchHeight._update();


		//ACF Maps
		// $('#acf-map').each(function(){
		// 	// create map
		// 	map = new_map( $(this) );
		// });


};



//Add blacklist to AJAX, so that backend links aren't hooked up.
function addBlacklistClass() {
    $( 'a' ).each( function() {
        if ( this.href.indexOf('/wp-admin/') !== -1 ||
			 this.href.indexOf('/wp-login.php') !== -1 ||
			 this.href.indexOf('replytocom') !== -1 ) {
            $( this ).addClass( 'wp-link' );
        }
    });
}


//Reinitialize contact form 7
function initContactForm() {
	$( 'div.wpcf7 > form' ).each( function() {
	    var $form = $( this );
	      	wpcf7.initForm( $form );
	    if ( wpcf7.cached ) {
	      	wpcf7.refill( $form );
	    }
	});
}



//Smoothstate - AJAX functionality
$(function() {

	'use strict';

	addBlacklistClass();

    var settings = {
    	debug: true,
    	prefetch: false,
    	cacheLength: 0,
        anchors: 'a',
        blacklist: 'form, .wp-link, .fancybox, .wpcf7-submit, .wpcf7-form',
        onStart: {
            duration: 800, // ms
            render: function ( $container ) {
            	//Close menu
            	$('#menu-toggle, #menu-slideout, html, body, .popup').removeClass( 'open' );

                $container.addClass( 'fade-out' );
                
            }
        },
        onReady: {
	      duration: 0,
	      render: function ($container, $newContent) {
	        // Remove your CSS animation reversing class
	        $container.removeClass('fade-out');

	        // Inject the new content
	        $container.html($newContent);

	      }
	    },
        onAfter: function($container, $newContent) {

        	initContactForm();
        	customScripts();

            var $hash = $( window.location.hash );

            if ( $hash.length !== 0 ) {
                var offsetTop = $hash.offset().top;
                $( '#main' ).animate( {
                        scrollTop: ( offsetTop ),
                    }, {
                        duration: 200
                });
            }
            
			$('#allcontent').removeClass( 'open' );
        	$container.addClass( 'fade-in' );

        },
    };

	$('#main').smoothState(settings).data('smoothState');
});
