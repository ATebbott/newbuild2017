'use strict';

// ///////////////////////////////////
// Required
// ///////////////////////////////////
var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'), 
    cleanCSS = require('gulp-clean-css'), 
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    autoprefixer = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    changed = require('gulp-changed'),
    ngAnnotate = require('gulp-ng-annotate'),
    reload = browserSync.reload;


// ///////////////////////////////////
// Init Browsersync
// ///////////////////////////////////
gulp.task('browser-sync', function(done) {
    browserSync.init({
      proxy: "http://localhost:8888/#####",
    });
    done();
});

  
// ///////////////////////////////////  
// Scripts
// ///////////////////////////////////
var jsPlugins = 'assets/js-plugins/*.js',
    jsCustom = 'assets/js-custom/*.js', 
    jsFiles = 'assets/js/*.js', 
    jsSrc = 'assets/js/', 
    jsDest = 'assets/js/dist';
    
gulp.task('scripts', function(done) {  
    gulp.src(jsPlugins)
        .pipe(concat('plugins.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsSrc))
         done();
});

gulp.task('customscripts', function(done) {  
  gulp.src(jsCustom)
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsSrc))
        .pipe(reload({ stream:true }));
         done();
});



// gulp.task('allscripts', function(done) {
//     return gulp.src(['./assets/js/plugins.js', './assets/js/main.js'])
//         .pipe(concat({ path: 'all.min.js' }))
//         .pipe(uglify())
//         .pipe(gulp.dest(jsDest))
//         .pipe(browserSync.stream())
//         done();
// });





// ///////////////////////////////////  
// Image minify
// ///////////////////////////////////  
gulp.task('imagemin', function (done) {
  gulp.src('assets/images/src/*')
      .pipe(changed('assets/images/dist'))
      .pipe(imagemin([
          imagemin.gifsicle({interlaced: true}),
          imagemin.jpegtran({progressive: true}),
          imagemin.optipng({optimizationLevel: 5}),
          imagemin.svgo({plugins: [{removeViewBox: true}]})
      ]))
      .pipe(gulp.dest('assets/images/dist'))
      .pipe( notify({ message: 'Imagemin task complete' }));
       done();
});



// ///////////////////////////////////  
// SASS
// ///////////////////////////////////

// Different options for the Sass tasks
var options = {};
options.sass = {
  errLogToConsole: true,
  precision: 8,
  noCache: true,
  //imagePath: 'assets/img'
};

options.sassmin = {
  errLogToConsole: true,
  precision: 8,
  noCache: true,
  outputStyle: 'compressed'
};

gulp.task('sass', function(done) {
    gulp.src('assets/sass/main.scss')
        .pipe(plumber())
        .pipe(sass(options.sass).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest('assets/css'))
        .pipe(notify({ title: 'Sass', message: 'sass task complete'  }));
         done();
});

// Sass-min
gulp.task('sass-min', function(done) {
    gulp.src('assets/sass/main.scss')
        .pipe(plumber())
        .pipe(sass(options.sassmin).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(rename( { suffix: '.min' } ) )
        .pipe(gulp.dest('assets/css'))
        .pipe(reload({ stream:true }))
        .pipe(notify({ title: 'Sass', message: 'sass-min task complete' }));
         done();
});



// Watchtask
gulp.task('watch', function(done) {
  gulp.watch( 'assets/images/src/*', gulp.series('imagemin'));
  gulp.watch( 'assets/sass/**/*.scss', gulp.series('sass', 'sass-min'));
  gulp.watch( 'assets/js-plugins/*.js', gulp.series('scripts', 'customscripts'));
  gulp.watch( 'assets/js-custom/custom.js', gulp.series('scripts', 'customscripts'));
  gulp.watch( 'assets/css/main.min.css' ).on('change', browserSync.reload);
  gulp.watch( '*.php' ).on('change', browserSync.reload);
  done();
});




// ///////////////////////////////////
// Default
// ///////////////////////////////////
gulp.task('default', gulp.parallel('browser-sync', 'watch'));